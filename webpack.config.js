/**
 * @author Emanuel Marsicovetere
 * @version 1.0.0
 */

var path = require('path');
const webpack = require('webpack');
const PRODUCTION = 'production'
const NODE_ENV = process.env.NODE_ENV
const isProduction = NODE_ENV === PRODUCTION

module.exports = {
    entry: !isProduction ? [
        'webpack-hot-middleware/client?reload=true',
        __dirname + '/src/client/index.js'
    ] : [
            __dirname + '/src/client/index.js'
        ],
    plugins: !isProduction ? [
        new webpack.HotModuleReplacementPlugin(),
    ] : '',
    module: {
        loaders: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                loader: isProduction ? 'babel-loader' : 'react-hot-loader!babel-loader',
            },
            {
                test   : /\.ttf/,
                loader: 'file-loader',
            },
             {
                 test   : /\.jpg/,
                 loader: 'file-loader',
             },
               {
                   test   : /\.png/,
                   loader: 'file-loader',
               }
        ]
    },
    resolve: {
        extensions: ['*', '.js', '.jsx']
    },
    output: {
        path: __dirname + '/public',
        publicPath: '/',
        filename: 'bundle.js'
    },
    devServer: {
        path: __dirname + '/public',
        hot: true
    }
};