const path = require('path')
const express = require('express')
const webpack = require('webpack')
const webpackMiddleware = require('webpack-dev-middleware')
const webpackHotMiddleware = require('webpack-hot-middleware')
const config = require('../../webpack.config.js')
var request = require('request');

const isDeveloping = process.env.NODE_ENV !== 'production'
const port = isDeveloping ? 3000 : process.env.PORT
const app = express()

var passport = require('passport');
var flash    = require('connect-flash');

var cookieParser = require('cookie-parser');
var bodyParser   = require('body-parser');
var session      = require('express-session');

const { Client } = require('pg')

const client = new Client({
    user: 'emma_admin',
    host: 'product.marsicovetere.ch',
    database: 'emma_product',
    password: 'rfQ4&37w',
    port: 5432,
})



require('./config/passport')(passport);


if (isDeveloping) {
    const compiler = webpack(config)
    const middleware = webpackMiddleware(compiler, {
        publicPath: config.output.publicPath,
        contentBase: 'src',
        stats: {
            colors: true,
            hash: false,
            timings: true,
            chunks: false,
            chunkModules: false,
            modules: false
        }
    })

    app.use(middleware)
    app.use(webpackHotMiddleware(compiler))
} else {
    app.use(express.static(path.resolve(__dirname, '../..', 'public/')))
}
app.use(cookieParser());
app.use(bodyParser());

// required for passport
app.use(session({ secret: 'ilovescotchscotchyscotchscotch' })); // session secret
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash());

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();
    res.redirect('/');
}

function isAlreadyLoggedIn(req, res, next) {
    if (!req.isAuthenticated())
        return next();
    res.redirect('/Main');
}

var jsonParser = bodyParser.json()
client.connect()

app.get('/', isAlreadyLoggedIn,  function response(req, res) {
    res.header("Content-Type", "text/html")
    res.sendFile(path.resolve(__dirname, '../..', 'public/index.html'))
})

app.get('/Main', isLoggedIn,  function response(req, res) {
    res.header("Content-Type", "text/html")
    res.sendFile(path.resolve(__dirname, '../..', 'public/index.html'))
})



app.post('/login', jsonParser, (req, res, next) => {
    res.setHeader('Content-Type', 'application/json');
    client.query(`SELECT * FROM admin_user WHERE name = '${req.body.username}'`, (err, data) => {
        if (data.rowCount === 1) {
            if (data.rows[0].password === req.body.password) {
                passport.authenticate('local-login', function(err, user, info) {
                    if (user) {
                        req.logIn(user, function(err) {
                            if (err) { return next(err); }
                            res.send(JSON.stringify({response: 'success'}));
                        });
                    }
                })(req, res, next);
            } else {
                res.send(JSON.stringify({response: 'error', message: 'wrong password'}));
            }
        } else {
            res.send(JSON.stringify({response: 'error', message: 'user does not exist'}));
        }
    })

})

app.listen(port, '0.0.0.0', function onStart(err) {
    if (err) {
        console.log(err)
    }
    console.info('==> Listening on port %s. Open up http://localhost:%s/ in your browser. Environment: ' + process.env.NODE_ENV, port, port)
})