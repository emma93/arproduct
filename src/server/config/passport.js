var LocalStrategy = require('passport-local').Strategy;

// var User = require('../../models/user');

const { Client } = require('pg')

const client = new Client({
    user: 'emma_admin',
    host: 'product.marsicovetere.ch',
    database: 'emma_product',
    password: 'rfQ4&37w',
    port: 5432,
})

client.connect()

module.exports = function(passport) {
    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    passport.deserializeUser(function(id, done) {
        client.query(`SELECT * FROM admin_user WHERE id = ${id}`, (err, user) => {
            done(err, user);
        })
    });

    passport.use('local-login', new LocalStrategy({
        // by default, local strategy uses username and password, we will override with email
        usernameField : 'username',
        passwordField : 'password',
        passReqToCallback : true // allows us to pass back the entire request to the callback
    },
    function(req, username, password, done) {
        /* User.findOne({ 'email' :  email }, function(err, user) {
            if (!user) {
                return done(null, false, req.flash('loginMessage', 'No user found.')); // req.flash is the way to set flashdata using connect-flash
            }
            if (!user.validPassword(password)) {
                return done(null, false, req.flash('loginMessage', 'Oops! Wrong password.')); // create the loginMessage and save it to session as flashdata
            }
            return done(null, user);
        }); */



        client.query(`SELECT * FROM admin_user WHERE name = '${username}'`, (err, data) => {
            if (data.rowCount === 1) {
                if (data.rows[0].password === req.body.password) {
                    return done(null, data.rows[0]);
                } else {
                    return done(null, false, req.flash('loginMessage', 'Oops! Wrong password.'));
                }
            } else {
                return done(null, false, req.flash('loginMessage', 'No user found.'));
            }
        })

    }));

};