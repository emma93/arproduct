const contentful = require('contentful')
const API_BASE_URL = 'https://cdn.contentful.com';

const SPACE_ID = '3dngbg6gckmj'
const ACCESS_TOKEN = '3f0956ebe47dbc5fa41ac27bfc740d8bb504a46f6d20b234598d2d6272a21c3a'

const client = contentful.createClient({
  space: SPACE_ID,
  accessToken: ACCESS_TOKEN
})

client.getEntry('4aaGiKQYTmIemGMGeSeu40')
.then(function (entry) {
  // logs the entry metadata
  console.log(entry.sys)

  // logs the field with ID title
  console.log(entry.fields.productName)
})