import axios from 'axios';

export function post(url, body='') {
 return fetch(url, {
    method: 'POST',
    headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
    },
    body: JSON.stringify(body)
 })
 .then((response) => {
   if(response.ok) {
     return response.json();
   } else {
     throw new Error('Server response wasn\'t OK');
   }
 })
 .then((json) => {
   return json;
 });
}

export function get(url) {
 return fetch(url, { method: 'GET'})
 .then((response) => {
   if(response.ok) {
     return response.json();
   } else {
     throw new Error('Server response wasn\'t OK');
   }
 })
 .then((json) => {
   return json;
 });
}

export function getImage(image) {
 return fetch(`${API_BASE_URL}/spaces/${SPACE_ID}/assets/${image.sys.id}?access_token=${ACCESS_TOKEN}`, {})
 .then((response) => {
   if(response.ok) {
     return response.json();
   } else {
     throw new Error('Server response wasn\'t OK');
   }
 })
 .then((json) => {
   return json;
 });
}