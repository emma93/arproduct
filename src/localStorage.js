export function getFromStorage(name) {
    return localStorage.getItem(name);
}

export function saveInStorage(name, value) {
    localStorage.setItem(name, value);
    return true;
}