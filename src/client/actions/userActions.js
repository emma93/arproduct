/**
 * @author Emanuel Marsicovetere
 * @version 1.0.0
 */

import axios from 'axios'

export function fetchUser () {
    return function (dispatch) {
        axios.get('https://reqres.in/api/users/2')
            .then((response) => {
                dispatch({type: 'FETCH_DATA_FULFILLED', payload: response.data})
            })
            .catch((err) => {
                dispatch({type: 'FETCH_DATA_REJECTED', payload: err})
            })
    }
}