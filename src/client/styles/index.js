/**
 * @author Emanuel Marsicovetere
 * @version 1.0.0
 */
import styled from 'styled-components'
import { injectGlobal } from 'styled-components'
import Gotham from './fonts/Gotham-Medium.ttf'
import GothamItalic from './fonts/Gotham-MediumItalic.ttf'


injectGlobal`
  @font-face {
    font-family: Gotham;
    src: url('${Gotham}') format('truetype');
  }
`