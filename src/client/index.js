/**
 * @author Emanuel Marsicovetere
 * @version 1.0.0
 */

import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'

ReactDOM.render(<App />, document.getElementById('app'))