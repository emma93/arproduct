/**
 * @author Emanuel Marsicovetere
 * @version 1.0.0
 */
import React, { Component } from 'react'
import Header from '../../components/layout/Header'
import Footer from '../../components/layout/Footer'
import SearchBar from '../../components/SearchBar'
import { MainCentered } from './styles'

export default class Main extends Component {
    constructor (props) {
        super(props);
    }



    render() {
        return (
            <div>
                <Header showNavi={false} />
                <MainCentered>
                    <SearchBar />
                </ MainCentered>
                <Footer />
            </div>
        )
    }
}