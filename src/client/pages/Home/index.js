/**
 * @author Emanuel Marsicovetere
 * @version 1.0.0
 */
import React, { Component } from 'react'
import axios from 'axios';
import Header from '../../components/layout/Header'
import Footer from '../../components/layout/Footer'
import { MainCentered } from './styles'
import { StyledInput, StyledButtonSmall } from '../../components/layout/Elements'

class Home extends Component {
    constructor() {
        super();
        this.state = {
            username: '',
            password: '',
        };
    }

    onChange = (e) => {
        const state = this.state
        state[e.target.name] = e.target.value;
        this.setState(state);
    }

    onSubmit = (e) => {
        e.preventDefault();
        const { username, password } = this.state;
        if (username !== '' && password !== '') {
            axios.post('/login', { username, password })
                .then((result) => {
                    if (result.data.response === 'success') {
                        window.location = '/Main';
                    }
                });
        }
    }

    render() {
        const {username, password} = this.state;
        return (
            <div>
                <Header showNavi={false} />
                {process.env.NODE_ENV}
                <MainCentered>
                    <form onSubmit={this.onSubmit} className="login-form">
                        <StyledInput type="text" name="username" value={username} placeholder="Username" onChange={this.onChange} />
                        <StyledInput type="text" name="password" value={password} placeholder="Password" onChange={this.onChange} />
                        <StyledButtonSmall color="red" type="submit">Login</StyledButtonSmall>
                    </form>
                </ MainCentered>
            </div>
        )
    }
}

export default Home