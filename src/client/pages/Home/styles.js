/**
 * @author Emanuel Marsicovetere
 * @version 1.0.0
 */
import styled from 'styled-components'

export const MainCentered = styled.div`
    max-width: 980px;
    padding: 15px;
    margin: 0 auto;

    .login-form {
        margin: 0 auto;
        width: 400px;
        text-align: center;
    }
`;