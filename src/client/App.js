/**
 * @author Emanuel Marsicovetere
 * @version 1.0.0
 */
import React, { Component } from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import Home from './pages/Home'
import Main from './pages/Main'

const contentful = require('contentful')

class App extends Component {

  render() {
    return (
      <Router>
        <div>
            <Route exact={true} path="/" component={ Home } />
            <Route exact={true} path="/Main" component={ Main } />
         </div>
      </Router>
    )
  }
}

export default App
