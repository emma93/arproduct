/**
 * @author Emanuel Marsicovetere
 * @version 1.0.0
 */
import styled from 'styled-components'

export const SPage = styled.div`
    .asdjhabds {
        sfdasfsdfsdf
    }
`;

export const StyledButton = styled.button`
    display: block;
    margin-top: 15px;
    width: 300px;
    height: 80px;
    border: 0;
    background-color: #e5e5e5;
`;

export const StyledButtonSmall = styled.button`
    display: block;
    margin-top: 15px;
    width: 300px;
    height: 35px;
    border: 0;
    background-color: #e5e5e5;
`;

export const AddButton = styled.button`
    display: block;
    width: 35px;
    height: 35px;
    margin-top: 22px;
    border: 0;
    background-color: #0e9648;
`;

export const DeleteButton = styled.button`
    display: block;
    width: 35px;
    height: 35px;
    border: 0;
    background-color: red;
`;

export const StyledInput = styled.input`
    display: block;
    margin-bottom: 10px;
    width: 300px;
    height: 35px;
    padding-left: 10px;
    border: 0;
    background-color: #e5e5e5;
`;

export const StyledArea = styled.textarea`
    display: block;
    margin-bottom: 10px;
    width: 100%;
    height: 150px;
    padding-left: 10px;
    border: 0;
    background-color: #e5e5e5;
`;

export const StyledLabel = styled.label`
    display: block;
    font-size: 12px;
`;