/**
 * @author Emanuel Marsicovetere
 * @version 1.0.0
 */
import React, { Component } from 'react'
import { HeaderStyled } from './styles'
import Navi from './Navi'

export default class Header extends Component {
    constructor (props) {
        super(props);
    }

    render() {
        const { showNavi } = this.props;
        return (
            <HeaderStyled>
                <a href="/"><div className="logo" /></a>
                { showNavi && <Navi /> }
            </HeaderStyled>
        )
    }
}