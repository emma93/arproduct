/**
 * @author Emanuel Marsicovetere
 * @version 1.0.0
 */
import styled from 'styled-components'
const logo = require('../../../../img/logo.png');

export const HeaderStyled = styled.div`
    width: 100%;
    height: 60px;
    background-color: #158007;
    box-sizing: border-box;
    padding-top: 10px;

    .logo {
        width: 200px;
        height: 40px;
        margin-left: 10px;
        background-image: url(${logo});
        background-size: 50%;
        background-repeat: no-repeat;
    }
`;