/**
 * @author Emanuel Marsicovetere
 * @version 1.0.0
 */
import React, { Component } from 'react'
import { StyledNavi } from './styles'

export default class Navi extends Component {
    constructor (props) {
        super(props);
    }

    render() {
        return (
            <StyledNavi>
                <ul>
                    <li><a href="/ContentTypes">Content Types</a></li>
                    <li><a href="/Content">Content</a></li>
                    <li><a href="/Media">Media</a></li>
                </ul>
            </StyledNavi>
        )
    }
}