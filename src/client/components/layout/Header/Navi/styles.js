/**
 * @author Emanuel Marsicovetere
 * @version 1.0.0
 */
import styled from 'styled-components'

export const StyledNavi = styled.div`
    width: 60%;
    height: 60px;
    position: absolute;
    left: 16.6%;
    top: 0;
    padding-top: 14px;

    ul {
        list-style-type: none;
        li {
            display: inline-block;
            margin-right: 80px;

            a {
                font-size: 20px;
                color: white;
            }
        }
    }
`;