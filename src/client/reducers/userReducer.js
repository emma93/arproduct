/**
 * @author Emanuel Marsicovetere
 * @version 1.0.0
 */
export default function reducer(state={
        user: {},
        fetching: false,
        fetched: false,
        error: null,
    }, action) {

    switch (action.type) {
        case 'FETCH_DATA': {
            return {...state, fetching: true}
        }
        case 'FETCH_DATA_REJECTED': {
            return {...state, fetching: false, error: action.payload}
        }
        case 'FETCH_DATA_FULFILLED': {
            return {
                ...state,
                fetching: false,
                fetched: true,
                user: action.payload.data,
            }
        }
    }
    return state
}