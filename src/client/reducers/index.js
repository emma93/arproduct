/**
 * @author Emanuel Marsicovetere
 * @version 1.0.0
 */
import { combineReducers } from 'redux'
import user from './userReducer'
import { i18nReducer } from 'react-redux-i18n'

export default combineReducers({
    user,
    i18n: i18nReducer
})