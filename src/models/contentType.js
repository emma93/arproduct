const mongoose = require('mongoose');

var ObjectId = mongoose.Schema.Types.ObjectId;

var contentType = new mongoose.Schema({
    _id: ObjectId,
    name: { type: String },
    fields: [],
    pageId: { type: String },
    status: { type: String }
}, { collection : 'contentType' });

module.exports = mongoose.model('contentType', contentType);