const mongoose = require('mongoose');

var ObjectId = mongoose.Schema.Types.ObjectId;

var page = new mongoose.Schema({
    _id: ObjectId,
    name: { type: String },
    created: { type: String }
}, { collection : 'page' });

module.exports = mongoose.model('page', page);