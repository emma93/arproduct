const mongoose = require('mongoose');
const bcrypt   = require('bcrypt-nodejs');

var ObjectId = mongoose.Schema.Types.ObjectId;

var userSchema = new mongoose.Schema({
    _id: ObjectId,
    firstname: { type: String },
    lastname: { type: String },
    username: { type: String },
    email: { type: String },
    passwort: { type: String },
    role: { type: Number }
}, { collection : 'user' });


userSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

userSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.passwort);
};

module.exports = mongoose.model('user', userSchema);