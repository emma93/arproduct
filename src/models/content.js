const mongoose = require('mongoose');

var ObjectId = mongoose.Schema.Types.ObjectId;

var content = new mongoose.Schema({
    _id: ObjectId,
    name: { type: String },
    fields: [],
    pageId: { type: String },
    status: { type: String },
    contentTypeId: { type: String }
}, { collection : 'content' });

module.exports = mongoose.model('content', content);